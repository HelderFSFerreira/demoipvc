FROM node:lts-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json yarn.lock ./
# COPY yarn.lock ./

# RUN npm install
# If you are building your code for production
RUN yarn install --frozen-lockfile

# Bundle app source
COPY . .

#Build
RUN yarn build

EXPOSE 3000
CMD [ "node", "dist/src/server.js" ]