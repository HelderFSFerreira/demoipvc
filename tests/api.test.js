
const request = require('supertest');
const app = require('../src/app');

describe('Get Euromillions', () => {
    it('Should have a response', async () => {
        const res = await request(app).get('/');
        
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('message');
    })
});


describe('Get Euromillions numbers with schema', () => {
    it('Should have numbers and stars properties', async () => {
        const res = await request(app).get('/');
        expect(res.body.message).toEqual(expect.objectContaining({
            numbers: expect.any(Array),
            stars: expect.any(Array),
        }));
    })
})

describe('Get Euromillions numbers', () => {
    it('Should be an array of 5 numbers', async () => {
        const res = await request(app).get('/');
        expect(res.body.message.numbers).toHaveLength(5);

        expect(res.body.message.numbers[1]).toEqual(expect.any(Number));
    })



})