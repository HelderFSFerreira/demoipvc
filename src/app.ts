import express from 'express';
import { EuroMillions } from './lib/euromillions';

const app = express();

app.get('/', (request, response) => {

    let numbers = EuroMillions.getNumbers();
    let stars = EuroMillions.getStars();

    return response.json({
        message: {
            numbers: numbers,
            stars: stars
        } 
    });
});

module.exports = app;