export class EuroMillions {
    static getNumbers(): number[] {
        var endList: number[] = [];

        do {
            endList.push(Math.floor(Math.random() * 50));
        } while (endList.length < 5);

        return endList;
    }

    static getStars(): number[] {
        var endList: number[] = [];

        do {
            endList.push(Math.floor(Math.random() * 11));
        } while (endList.length < 2);

        return endList;
    }

}